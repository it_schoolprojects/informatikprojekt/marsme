import marsme.datastructures.queue.QOverflowException;
import marsme.datastructures.queue.QUnderflowException;
import marsme.datastructures.queue.Queue;
import org.junit.Assert;
import org.junit.Test;

public final class QueueTest {

    @Test
    public void testReadAndWrite() {
        final Queue<String> queue = new Queue<>(5, String.class);
        //Test Insert
        queue.add("1");
        queue.add("2");
        queue.add("3");
        queue.add("4");
        queue.add("5");
        //Test read
        Assert.assertEquals(queue.read(), "1");
        Assert.assertEquals(queue.read(), "2");
        //Test insert new ones
        queue.add("6");
        queue.add("7");
        //Test reading rest
        for (int i = 3; i <= 7; i++) {
            final String iAsString = Integer.toString(i);
            Assert.assertEquals(queue.read(), iAsString);
        }
    }

    @Test
    public void testQueries() {
        final Queue<String> queue = new Queue<>(2, String.class);
        //Test empty
        assert queue.isEmpty();
        assert !queue.hasNext();
        assert !queue.isFull();
        //Test semi filled
        queue.add("Test");
        assert !queue.isEmpty();
        assert queue.hasNext();
        assert !queue.isFull();
        //Test full
        queue.add("Test");
        assert !queue.isEmpty();
        assert queue.hasNext();
        assert queue.isFull();
    }

    @Test(expected = QUnderflowException.class)
    public void testUnderflow() {
        final Queue<String> queue = new Queue<>(10, String.class);
        queue.read();
    }

    @Test(expected = QOverflowException.class)
    public void testOverflow() {
        final Queue<String> queue = new Queue<>(1, String.class);
        try {
            queue.add("OK");
        } catch (QOverflowException e) {
            assert false;
        }
        queue.add("Overflow");
    }

    @Test
    public void testIterator() {
        final Queue<String> queue = new Queue<>(3, String.class);
        queue.add("1");
        queue.add("2");
        int counter = 0;
        for (String s : queue) {
            counter++;
            if (counter == 1) {
                Assert.assertEquals(s, "1");
            } else {
                Assert.assertEquals(s, "2");
            }
        }
        assert counter == 2;
    }

}
