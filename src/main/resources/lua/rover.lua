goingToMetal = false
avoidingAlien = false

function Idle()
avoidingAlien = false
goingToMetal = false
     if rover:isLoaded() then
          rover:moveToGarage()
     else
          rover:search()
     end
end

function MetalOnSight(x,y)
     if rover:isLoaded() or avoidingAlien then return end
     if goingToMetal and rover:isOnOreField()then 
          rover:mine()
          rover:moveToGarage()
          return
     end
     if not goingToMetal then
          rover:moveTo(x, y)
          goingToMetal = true
     end

end

function AlienOnSight(x,y)
     pos = rover:getPosition()
     deltaX = pos[1] - x
     deltaY = pos[2] - y
     length = (deltaX^2 + deltaY^2)^0.5
     deltaX = deltaX * (200 / length)
     deltaY = deltaY * (200 / length)
     rover:moveTo(pos[1] + deltaX, pos[2] + deltaY)
     avoidingAlien=true
     goingToMetal=false
end

function LowBattery()
rover:moveToGarage()
end

function ReceiveData(data)
end