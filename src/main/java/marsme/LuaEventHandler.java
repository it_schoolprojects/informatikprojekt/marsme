package marsme;

import marsme.entities.Rover.LuaVM;

/**
 * Klasse fuer das Managen von Rovern und deren LuaVMs
 *
 * @author Henrik
 * @version 1.0.0
 */

final class LuaEventHandler {
    /**
     * Fuehrt den Lua Code fuer ein spezfisches Event aus
     *
     * @param event Die Event Datenstruktur
     * @param vm    Die LuaVM welche zur Ausfuehrung des evenspezifischen codes genutzt wird
     */
    static void handleEvent(GameEvent event, LuaVM vm) {
        switch (event.getEventType()) {
            case 1:
                vm.getGlobals().load("Idle()").call();
                break;
            case 2:
                vm.getGlobals().load("LowBattery()").call();
                break;
            case 3:
                vm.getGlobals().load("MetalOnSight(" + event.getX() + "," + event.getY() + ")").call();
                break;
            case 4:
                vm.getGlobals().load("AlienOnSight(" + event.getX() + "," + event.getY() + ")").call();
                break;
            case 5:
                vm.getGlobals().load("DataReceived(" + event.getData() + ")").call();
                break;
        }
    }
}
