package marsme;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

/**
 * @author Jan, Eric
 * @version 1.0.0
 */
public enum Assets {
    ROVER_IMAGE("/img/rover.png"),
    ALIEN_IMAGE("/img/alien.png"),
    GARAGE_IMAGE("/img/garage.png"),
    METAL_IMAGE("/img/metal.png"),

    SETTIGNS_IMAGE("/img/settings.png"),

    MAINICON_16x16("/icons/main_icon_16x16.png"),
    MAINICON_32x32("/icons/main_icon_32x32.png"),
    MAINICON_64x64("/icons/main_icon_64x64.png"),

    EDITORICON_16x16("/icons/editor_icon_16x16.png"),
    EDITORICON_32x32("/icons/editor_icon_32x32.png"),
    EDITORICON_64x64("/icons/editor_icon_64x64.png"),

    CONSOLEICON_16x16("/icons/console_16x16.png"),
    CONSOLEICON_32x32("/icons/console_32x32.png"),
    CONSOLEICON_64x64("/icons/console_64x64.png"),

    STARTSCREEN("/img/start_screen"),
    GAMEOVER_SCREEN("/img/gameOver_screen.jpg"),

    GAMESTART_SOUND("/sounds/gameStart_sound.mp3"),
    COLLECTINGHEAP_SOUND("/sounds/collectingHeap_sound.mp3"),
    ROVERDESTROYED_SOUND("/sounds/roverDestroyed_sound.mp3"),
    GAMEOVER_SOUND("/sounds/gameOver_sound.mp3");

    private final String path;

    Assets(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public File getFile() {
        return new File(path);
    }

    public URL getResource() {
        return getClass().getResource(path);
    }

    public InputStream getResAsStream() {
        return getClass().getResourceAsStream(path);
    }

}
