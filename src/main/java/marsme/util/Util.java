package marsme.util;

/**
 * @author Jan, Henrik
 * @version 1.0.0
 */
@SuppressWarnings("WeakerAccess")
public final class Util {

    private Util() {

    }

    public static double cos(double angle) {
        angle = Math.toRadians(angle);
        return Math.cos(angle);
    }

    public static double sin(double angle) {
        angle = Math.toRadians(angle);
        return Math.sin(angle);
    }

    public static double acos(double val) {
        double angle = Math.acos(val);
        return Math.toDegrees(angle);
    }

    public static double random(double min, double max) {
        return Math.random() * (max - min) + min;
    }


    /**
     * Berechnet die Distanz zwischen zwei Punkten
     *
     * @param a Punkt a
     * @param b Punkt b
     * @return Die Distanz
     */
    private static double getDistanceBetweenPoints(Vector2D a, Vector2D b) {
        return Math.sqrt(Math.pow(a.getX() - b.getX(), 2.0) + Math.pow(a.getY() - b.getY(), 2.0));
    }

    /**
     * @param point       Der Ortsvektor des zu ueberpruefenden Vektors
     * @param sectorStart Der Ortsvektor des Punktes der den Start des Sektors definiert
     * @param sectorEnd   Der Ortsvektor des Punktes der das Ende des Sektors definiert
     * @param center      Der Ortsvektor des Mittelpunktes des Kreises aka Position des Rovers
     * @return Ob point in dem Kreissektor definiert durch center, sectorStart und SectorEnd liegt
     */
    public static boolean pointInViewRange(Vector2D point, Vector2D sectorStart, Vector2D sectorEnd, Vector2D center) {
        if (getDistanceBetweenPoints(center, point) <= getDistanceBetweenPoints(center, sectorStart)) {
            double a = Vector2D.getAngle(new Vector2D(sectorStart.getX() - center.getX(), sectorStart.getY() - center.getY()), new Vector2D(point.getX() - center.getX(), point.getY() - center.getY()));
            double b = Vector2D.getAngle(new Vector2D(sectorEnd.getX() - center.getX(), sectorEnd.getY() - center.getY()), new Vector2D(point.getX() - center.getX(), point.getY() - center.getY()));
            double c = Vector2D.getAngle(new Vector2D(sectorStart.getX() - center.getX(), sectorStart.getY() - center.getY()), new Vector2D(sectorEnd.getX() - center.getX(), sectorEnd.getY() - center.getY()));
            return doubleEquals(a + b, c);
        }
        return false;
    }

    public static boolean doubleEquals(double a, double b) {
        return Math.abs(a - b) < 0.001;
    }

    public static double angle360(Vector2D v) {
        double angle = Vector2D.getAngle(v, new Vector2D(1, 0));
        if (Vector2D.scalarProduct(v, new Vector2D(0, 1)) < 0) {
            angle = 360 - angle;
        }
        return angle % 360;
    }

    public static double amountPerTick(double val, double timeDiff) {
        return (val * timeDiff);
    }

    @SuppressWarnings("Duplicates")
    public static double gradualRotation(double rotation, double target, double rotationVel, double timeDiff) {
        double deltaRotation = target - rotation;
        if (Math.abs(deltaRotation) < Util.amountPerTick(rotationVel, timeDiff)) {
            return target;
        } else if (deltaRotation > 0) {
            if (Math.abs(target - rotation - 360) < deltaRotation) {
                return rotation - amountPerTick(rotationVel, timeDiff);
            } else {
                return rotation + amountPerTick(rotationVel, timeDiff);
            }
        } else {
            if (Math.abs((360 + target) - rotation) < -deltaRotation) {
                return rotation + amountPerTick(rotationVel, timeDiff);
            } else {
                return rotation - amountPerTick(rotationVel, timeDiff);
            }
        }
    }

    public static Vector2D gradualTranslation(Vector2D position, double vel, double rotation, double timeDiff) {
        return Vector2D.addition(position, Vector2D.generateRotatedVector(new Vector2D(amountPerTick(vel, timeDiff), 0), rotation));
    }
}
