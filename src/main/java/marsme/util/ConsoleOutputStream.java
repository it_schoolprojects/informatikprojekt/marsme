package marsme.util;

import marsme.gui.ConsoleWindow;

import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

final class ConsoleOutputStream extends OutputStream {

    private static final PrintStream STDOUT = System.out;
    private final ConsoleWindow cw;

    ConsoleOutputStream(ConsoleWindow cw) {
        this.cw = cw;
    }

    @Override
    public void write(int b) {
        cw.print(String.valueOf((char) b));
        STDOUT.write(b);
    }

    @Override
    public void close() throws IOException {
        super.close();
        cw.dispatchEvent(new WindowEvent(cw, WindowEvent.WINDOW_CLOSING));
    }

    void show() {
        cw.setVisible(true);
        cw.toFront();
        cw.requestFocus();
    }
}
