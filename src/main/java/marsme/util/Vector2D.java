package marsme.util;

/**
 * Thread safe vectors
 *
 * @author Jan
 * @version 1.0.0
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class Vector2D {
    private final double x, y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static Vector2D nullVector() {
        return new Vector2D(0, 0);
    }

    public static Vector2D subtraction(Vector2D minuend, Vector2D subtrahend) {
        double vX = minuend.x - subtrahend.x;
        double vY = minuend.y - subtrahend.y;
        return new Vector2D(vX, vY);
    }

    public static Vector2D addition(Vector2D v1, Vector2D v2) {
        double x = v1.x + v2.x;
        double y = v1.y + v2.y;
        return new Vector2D(x, y);
    }

    public static Vector2D unitVector(Vector2D v) {
        double length = v.getLength();
        return generateScaledVector(v, 1 / length);
    }

    public static Vector2D generateScaledVector(Vector2D v, double scale) {
        double scaledX = v.x * scale;
        double scaleY = v.y * scale;
        return new Vector2D(scaledX, scaleY);
    }

    public static Vector2D generateRotatedVector(Vector2D v, double rotation) {
        double newX = v.x * Util.cos(rotation) - v.y * Util.sin(rotation);
        double newY = v.x * Util.sin(rotation) + v.y * Util.cos(rotation);
        return new Vector2D(newX, newY);
    }

    public static Vector2D setScale(Vector2D v, double scale) {
        return generateScaledVector(unitVector(v), scale);
    }


    public static double scalarProduct(Vector2D v1, Vector2D v2) {
        return v1.x * v2.x + v1.y * v2.y;
    }

    public static double getAngle(Vector2D v1, Vector2D v2) {
        return Util.acos(scalarProduct(v1, v2) / (v1.getLength() * v2.getLength()));
    }

    public static Vector2D getNormal(Vector2D v) {
        if (v.y == 0) {
            return new Vector2D(0, 1);
        }
        double newX = 1;
        double newY = -v.x / v.y;
        Vector2D normal = new Vector2D(newX, newY);
        return Vector2D.unitVector(normal);
    }

    public static Vector2D projection(Vector2D p, Vector2D a) {
        double scale = scalarProduct(a, p) / scalarProduct(a, a);
        return Vector2D.generateScaledVector(a, scale);
    }

    public static Vector2D getMiddleVector(Vector2D a, Vector2D b) {
        return generateScaledVector(addition(a, b), 0.5);
    }

    public double getLength() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double[] asArray() {
        double[] val = new double[2];
        val[0] = getX();
        val[1] = getY();
        return val;
    }

    public int[] asArrayRounded() {
        int[] val = new int[2];
        val[0] = (int) getX();
        val[1] = (int) getY();
        return val;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Vector2D)) {
            return false;
        }
        Vector2D v = (Vector2D) o;
        return Util.doubleEquals(this.x, v.x) &&
                Util.doubleEquals(this.y, v.y);
    }
}
