package marsme.util;

import marsme.gui.ConsoleWindow;

import java.io.PrintStream;

public final class HookedPrintStream extends PrintStream {

    public HookedPrintStream() {
        super(new ConsoleOutputStream(new ConsoleWindow()), true);
    }

    public void show() {
        ((ConsoleOutputStream) out).show();
    }

}
