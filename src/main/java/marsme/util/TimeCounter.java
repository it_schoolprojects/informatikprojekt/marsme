package marsme.util;

public final class TimeCounter {
    private int seconds = 0;
    private int minutes = 0;
    private int hours = 0;

    public void count() {
        seconds++;
        if (seconds == 60) {
            seconds = 0;
            minutes++;
        }
        if (minutes == 60) {
            minutes = 0;
            hours++;
        }
        checkIntegrity();
    }

    @Override
    public String toString() {
        return hours != 0 ? String.format("%02dh:%02dmin:%02ds", hours, minutes, seconds) :
                minutes != 0 ? String.format("%02dmin:%02ds", minutes, seconds) :
                        String.format("%02ds", seconds);
    }

    private void checkIntegrity() {
        if (seconds > 60 || seconds < 0 || minutes > 60 || minutes < 0 || hours < 0) {
            throw new IllegalStateException();
        }
    }
}
