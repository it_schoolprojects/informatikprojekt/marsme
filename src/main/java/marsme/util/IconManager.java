package marsme.util;

import javafx.scene.image.Image;
import marsme.Assets;

import java.awt.*;

public final class IconManager {


    public static final Image[] mainIcons = new Image[3];
    public static final java.awt.Image[] editorIcons = new java.awt.Image[3];
    public static final java.awt.Image[] consoleIcons = new java.awt.Image[3];

    static {
        Image smallMainIcon = new Image(Assets.MAINICON_16x16.getResAsStream());
        Image normalMainIcon = new Image(Assets.MAINICON_32x32.getResAsStream());
        Image bigMainIcon = new Image(Assets.MAINICON_64x64.getResAsStream());


        mainIcons[0] = smallMainIcon;
        mainIcons[1] = normalMainIcon;
        mainIcons[2] = bigMainIcon;

        Toolkit tk = Toolkit.getDefaultToolkit();
        editorIcons[0] = tk.getImage(Assets.EDITORICON_16x16.getResource());
        editorIcons[1] = tk.getImage(Assets.EDITORICON_32x32.getResource());
        editorIcons[2] = tk.getImage(Assets.EDITORICON_64x64.getResource());

        consoleIcons[0] = tk.getImage(Assets.CONSOLEICON_16x16.getResource());
        consoleIcons[1] = tk.getImage(Assets.CONSOLEICON_32x32.getResource());
        consoleIcons[2] = tk.getImage(Assets.CONSOLEICON_64x64.getResource());

    }

}
