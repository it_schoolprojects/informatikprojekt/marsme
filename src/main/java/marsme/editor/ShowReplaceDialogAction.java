package marsme.editor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Diese Klasse stellt ein MenuItem dar. Sie wird genutzt, um ein vom Nutzer gewaehltes Wort bzw Phrase
 * zu ersetzen.
 *
 * @author Victor
 * @version 1.0.0
 */

class ShowReplaceDialogAction extends AbstractAction {

    private final Editor edit;

    ShowReplaceDialogAction(Editor edit) {
        super("Replace...");
        this.edit = edit;
        int c = edit.getToolkit().getMenuShortcutKeyMask();
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R, c));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        edit.getReplaceDialog().setVisible(true);
    }

}