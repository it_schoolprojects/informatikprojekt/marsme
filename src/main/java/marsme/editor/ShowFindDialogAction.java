package marsme.editor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Diese Klasse stellt ein MenuItem dar. Sie wird genutzt, um ein vom Nutzer gewaehltes Wort bzw Phrase
 * zu finden.
 *
 * @author Victor
 * @version 1.0.0
 */

class ShowFindDialogAction extends AbstractAction {

    private final Editor edit;

    ShowFindDialogAction(Editor edit) {
        super("Find...");
        this.edit = edit;
        int c = edit.getToolkit().getMenuShortcutKeyMask();
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F, c));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        edit.getFindDialog().setVisible(true);
    }

}