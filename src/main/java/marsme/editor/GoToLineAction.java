package marsme.editor;

import org.fife.rsta.ui.GoToDialog;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Diese Klasse stellt ein MenuItem dar. Sie wird genutzt, um den Cursor in eine vom Nutzer gewaehlte
 * Zeile zu verschieben.
 *
 * @author Victor
 * @version 1.0.0
 */

class GoToLineAction extends AbstractAction {
    private final Editor edit;

    GoToLineAction(Editor edit) {
        super("Go To Line...");
        this.edit = edit;
        int c = edit.getToolkit().getMenuShortcutKeyMask();
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_L, c));
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        //Der Nutzer wird um die Nummer der Zeile gebeten, in die der Cursor gebracht werden soll.
        GoToDialog dialog = new GoToDialog(edit);
        dialog.setMaxLineNumberAllowed(edit.textField.getLineCount());
        dialog.setVisible(true);
        int line = dialog.getLineNumber();

        //Der Cursor wird in die angegebene Zeile gesetzt.
        if (line > 0) {
            try {
                edit.textField.setCaretPosition(edit.textField.getLineStartOffset(line - 1));
            } catch (BadLocationException ble) { // Never happens
                UIManager.getLookAndFeel().provideErrorFeedback(edit.textField);
                ble.printStackTrace();
            }
        }
    }

}
