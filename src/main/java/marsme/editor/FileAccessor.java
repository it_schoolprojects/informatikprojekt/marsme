package marsme.editor;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.util.Scanner;


/**
 * Diese Klasse nutzt den vom Nutzer eingegebenen String und speichert diesen in einer .lua Datei ab.
 * Ausserdem kann er diese  bei Neustart oeffnen und in den Editor einlesen.
 *
 * @author Victor
 * @version 1.0.0
 */

class FileAccessor {
    private final Editor edit;
    private String path;

    FileAccessor(Editor edit) {
        this.edit = edit;
    }

    /**
     * Diese Methode speichert den im Editor eingegebenen Code in einer .lua Datei.
     */
    void saveAt() {
        edit.codeRefresh();
        String txt = edit.getCode();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("/home/me/Desktop"));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("LUA-Code", "lua");
        fileChooser.setFileFilter(filter);

        if (fileChooser.showSaveDialog(edit) == JFileChooser.APPROVE_OPTION) {
            try {
                BufferedWriter writer;

                /*Es wird ueberprueft, ob die Datei schon ein .lua Endung hat.
                 * Anschliessend wird die Datei gespeichert
                 */
                if (fileChooser.getSelectedFile().getName().endsWith(".lua")) {
                    writer = new BufferedWriter(new FileWriter(fileChooser.getSelectedFile()));
                } else {
                    writer = new BufferedWriter(new FileWriter(fileChooser.getSelectedFile() + ".lua"));
                }

                path = fileChooser.getSelectedFile().getPath();
                writer.write(txt);
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Diese Methode speichert die Aenderungen der Datei, falls eine geoeffnet wurde.
     * Daher ist der Nutzer nicht gezwungen, jedes Mal die Datei "neu" zu speicher bzw. dann die alte zu ueberschreiben.
     */
    void save() {
        edit.codeRefresh();
        String txt = edit.getCode();

        //Es wird ueberprueft, ob die Datei schon einmal eingelesen bzw gespeichert wurde. Wenn ja wird diese alte Datei ersetzt.
        if (path != null) {
            BufferedWriter bw = null;
            FileWriter fw = null;
            try {
                fw = new FileWriter(path);
                bw = new BufferedWriter(fw);
                bw.write(txt);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bw != null) bw.close();
                    if (fw != null) fw.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            saveAt();
        }
    }


    /**
     * Diese Methode oeffnet die vom Nutzer ausgewaehlte Datei in den Editor
     *
     * @throws FileNotFoundException durch oeffnen von nicht existenten Dateien
     */
    void open() throws FileNotFoundException {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("/home/me/Desktop"));

        if (fileChooser.showOpenDialog(edit) == JFileChooser.APPROVE_OPTION) {
            java.io.File file = fileChooser.getSelectedFile();
            Scanner input = new Scanner(file);
            edit.textField.setText("");
            if (file.getName().endsWith(".lua")) {
                //Es wird die vom Nutzer gewaehlte Datei eingelesen und der Text dann in den Editor gegeben.
                while (input.hasNext()) {
                    edit.textField.append(input.nextLine());
                    edit.textField.append("\n");
                }
            } else {
                edit.textField.setText("File format isn't supported");
            }
            path = fileChooser.getSelectedFile().getPath();
        }
    }

}
