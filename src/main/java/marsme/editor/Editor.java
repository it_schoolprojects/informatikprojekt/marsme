package marsme.editor;

import marsme.gui.MainController;
import marsme.util.IconManager;
import org.fife.rsta.ui.CollapsibleSectionPanel;
import org.fife.rsta.ui.SizeGripIcon;
import org.fife.rsta.ui.search.FindDialog;
import org.fife.rsta.ui.search.ReplaceDialog;
import org.fife.rsta.ui.search.SearchEvent;
import org.fife.rsta.ui.search.SearchListener;
import org.fife.ui.rsyntaxtextarea.ErrorStrip;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.fife.ui.rtextarea.SearchContext;
import org.fife.ui.rtextarea.SearchEngine;
import org.fife.ui.rtextarea.SearchResult;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Arrays;

/**
 * Diese Klasse nutzt einen JFrame, bei dem der Benutzer seinen Code
 * eingeben kann. Dieser wird temporaer in dieser Klasse gespeichert.
 * Ausserdem kann der Code in einer txt-File gespeichert werden,
 * damit der Nutzer nicht jedes mal den Code neu schreiben muss.
 *
 * @author Victor
 */
public class Editor extends JFrame implements SearchListener {

    /**
     * Das Code Textfeld mit automatischem Syntaxhighlighting
     */
    public final RSyntaxTextArea textField;
    /**
     * @see MainController
     */
    private final MainController ctrl;
    /**
     * Die Statusleiste am Boden des Editors
     */
    private final StatusBar statusBar;
    /**
     * Aktuell im Textfeld stehender Code
     */
    private String code;

    /**
     * Der Textfinder
     */
    private FindDialog findDialog = new FindDialog(this, this);
    /**
     * Der Textersetzer
     */
    private ReplaceDialog replaceDialog = new ReplaceDialog(this, this);


    /**
     * Enthaelt den gesamten Input seit der letzten Autocompleteaktion
     *
     * @param ctrl Der Maincontroller
     */

    public Editor(MainController ctrl) {
        super("Rover-Programmer");
        this.ctrl = ctrl;

        setIconImages(Arrays.asList(IconManager.editorIcons));

        // Default Code um eine stabilen Start auch ohne benutzerdefinierten Code zu gewaehrleisten
        code = "function Idle()\nend\n\n" +
                "function MetalOnSight(x,y)\nend\n\n" +
                "function AlienOnSight(x,y)\nend\n\n" +
                "function LowBattery()\nend\n\n" +
                "function ReceiveData(data)\nend\n\n";

        textField = new RSyntaxTextArea(50, 75);
        ErrorStrip errorStrip = new ErrorStrip(textField);
        textField.append(code);
        setTitle("Rover-Programmer");

        JPanel panel = new JPanel(new BorderLayout());

        RTextScrollPane scrollBar = new RTextScrollPane(textField);

        CollapsibleSectionPanel collapsibleSectionPanel = new CollapsibleSectionPanel();

        statusBar = new StatusBar();

        MenuBar menuBar = new MenuBar(this);
        textField.setTabsEmulated(true);

        // Anwenden des systemweiten Look and Feel auf die Swing Applikation
        if (System.getProperty("os.name").toLowerCase().contains("win") ||
                System.getProperty("os.name").toLowerCase().contains("mac")) {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        /*
         * Ein Documentlistner wird erzeugt, der jedes Mal wenn sich das Documant veraendert, die Variable "code"
         * aktualisiert
         */
        DocumentListener documentListener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                codeRefresh();
                Editor.this.setTitle("Rover-Programmer *");
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                codeRefresh();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                codeRefresh();
            }
        };

        KeyListener autocompleter = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

                if (e.getKeyChar() == '(') {
                    e.consume();
                    Editor.this.textField.insert("()", Editor.this.textField.getCaretPosition());
                    Editor.this.textField.setCaretPosition(Editor.this.textField.getCaretPosition() - 1);
                } else if (e.getKeyChar() == '"') {
                    e.consume();
                    Editor.this.textField.insert("\"\"", Editor.this.textField.getCaretPosition());
                    Editor.this.textField.setCaretPosition(Editor.this.textField.getCaretPosition() - 1);
                } else if (e.getKeyChar() == '{') {
                    e.consume();
                    Editor.this.textField.insert("{}", Editor.this.textField.getCaretPosition());
                    Editor.this.textField.setCaretPosition(Editor.this.textField.getCaretPosition() - 1);
                } else if (e.getKeyChar() == '\'') {
                    e.consume();
                    Editor.this.textField.insert("''", Editor.this.textField.getCaretPosition());
                    Editor.this.textField.setCaretPosition(Editor.this.textField.getCaretPosition() - 1);
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        };

        textField.getDocument().addDocumentListener(documentListener);
        textField.addKeyListener(autocompleter);
        textField.setTabsEmulated(false);

        //Die Schriftart wird fuer das Textfeld gesetzt.
        Font font = new Font(
                Font.MONOSPACED,
                Font.PLAIN,
                textField.getFont().getSize());
        textField.setFont(font);

        /*
         * Alle oben erzeugten Objekte werden zu einem Editor zusammengefuegt und dann wird das Fenster sichtbargemacht
         * und umbenannt. Zudem wird das Syntaxhighlighting initiert auf die Sprache "Lua".
         */

        setContentPane(panel);
        setJMenuBar(menuBar);
        textField.setMarkOccurrences(true);
        collapsibleSectionPanel.add(scrollBar);
        panel.add(errorStrip, BorderLayout.LINE_END);
        panel.add(statusBar, BorderLayout.SOUTH);
        panel.add(collapsibleSectionPanel);
        textField.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_LUA);
        textField.setCodeFoldingEnabled(true);
        panel.add(scrollBar);
        this.setJMenuBar(menuBar);
        pack();
        setLocationRelativeTo(null);
    }

    public static void main(String[] args) {
        new Editor(new MainController()).setVisible(true);
    }

    /**
     * Beobachtet die im Menubar erzeugten Dialoge, fuerht die Suche aus, wenn der Nutzer durch den Dialog
     * etwas sucht.
     *
     * @param e Der zu suchende Text
     */
    public void searchEvent(SearchEvent e) {
        SearchEvent.Type type = e.getType();
        SearchContext context = e.getSearchContext();
        SearchResult result;

        //Unterscheidet zwischen den versch. Arten an Suchanfragen
        switch (type) {
            default:
            case MARK_ALL:
                result = SearchEngine.markAll(textField, context);
                break;
            case FIND:
                result = SearchEngine.find(textField, context);
                if (!result.wasFound()) {
                    UIManager.getLookAndFeel().provideErrorFeedback(textField);
                }
                break;
            case REPLACE:
                result = SearchEngine.replace(textField, context);
                if (!result.wasFound()) {
                    UIManager.getLookAndFeel().provideErrorFeedback(textField);
                }
                break;
            case REPLACE_ALL:
                result = SearchEngine.replaceAll(textField, context);
                JOptionPane.showMessageDialog(null, result.getCount() +
                        " occurrences replaced.");
                break;
        }

        String text;

        if (result.wasFound()) {
            text = "Text found; occurrences marked: " + result.getMarkedCount();
        } else if (type == SearchEvent.Type.MARK_ALL) {
            if (result.getMarkedCount() > 0) {
                text = "Occurrences marked: " + result.getMarkedCount();
            } else {
                text = "";
            }
        } else {
            text = "Text not found";
        }

        statusBar.setLabel(text);
    }

    public String getSelectedText() {
        return textField.getSelectedText();
    }

    void codeRefresh() {
        code = textField.getText();
        ctrl.setSaveCode(code);
    }

    public String getCode() {
        code = textField.getText();
        return code;
    }

    FindDialog getFindDialog() {
        return findDialog;
    }

    ReplaceDialog getReplaceDialog() {
        return replaceDialog;
    }


    /**
     * Erstellt im Dialogfenster eine kleine Statusanzeige.
     */
    private static class StatusBar extends JPanel {

        private final JLabel label;

        private StatusBar() {
            label = new JLabel("Ready");
            setLayout(new BorderLayout());
            add(label, BorderLayout.LINE_START);
            add(new JLabel(new SizeGripIcon()), BorderLayout.LINE_END);
        }

        private void setLabel(String label) {
            this.label.setText(label);
        }

    }

}