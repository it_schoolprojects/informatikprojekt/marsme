package marsme.editor;


import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import marsme.util.IconManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;

/**
 * Diese Klasse stellt die Menueleiste dar. Sie hat einen Reiter fuer "Speichern" und "Loeschen"
 *
 * @author Victor
 * @version 1.0.0
 */

class MenuBar extends JMenuBar {
    private final FileAccessor fileAccessor;

    MenuBar(Editor edit) {
        super();
        fileAccessor = new FileAccessor(edit);
        //Reiter "File" wird erstellt
        JMenu FileMenu = new JMenu("File");

        //MenuItem "SaveAt" wird erzeugt. Er laesst den Nutzer etwas unter einem angegebenen Pfad speichern.
        JMenuItem saveAt = new JMenuItem(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                fileAccessor.saveAt();
            }
        });
        saveAt.setText("Save Code at");
        FileMenu.add(saveAt);

        //MenuItem "Save" wird erzeugt. Er laesst den Nutzer seine Datei speichern.
        JMenuItem save = new JMenuItem(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                fileAccessor.save();
            }
        });
        save.setText("Save Code ");
        FileMenu.add(save);

        //MenuItem "open" wird erzeugt. Er laesst den Nutzer eine Datei unter einem angegebenen Pfad oeffnen.
        JMenuItem open = new JMenuItem(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    fileAccessor.open();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        open.setText("" +
                "Open Code");
        FileMenu.add(open);

        //Der Reiter "Search" wird erstellt.
        JMenu searchMenu = new JMenu("Search");

        //Die MenuItems zum Suchen, Ersetzen oder werden erstellt und in den Reiter "Search" eingefuegt.
        searchMenu.add(new JMenuItem(new ShowFindDialogAction(edit)));
        searchMenu.add(new JMenuItem(new ShowReplaceDialogAction(edit)));
        searchMenu.add(new JMenuItem(new GoToLineAction(edit)));

        //Es werden Shortcuts fuer die obenerstellten MenuItems erstellt und zugewiesen.
        open.setAccelerator(KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        save.setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));

        //Der Reiter "File" wird in die MenuLeiste eingesetzt.
        this.add(FileMenu);
        this.add(searchMenu);

        JButton help = new JButton();
        help.addActionListener(new ActionListener() {
            private final JFrame helpFrame = new JFrame();

            {
                JFXPanel jfx = new JFXPanel();
                try {
                    jfx.setScene(
                            new Scene(
                                    FXMLLoader.load(getClass().getResource("/fx/help.fxml"))
                            )
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }
                helpFrame.add(jfx);
                helpFrame.setSize(550, 650);
                helpFrame.setResizable(false);
                helpFrame.setTitle("Help");
                helpFrame.setIconImages(Arrays.asList(IconManager.editorIcons));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                if (helpFrame.isVisible()) {
                    helpFrame.requestFocus();
                    return;
                }
                helpFrame.setVisible(true);
            }
        });
        help.setText("Help");
        this.add(help);
    }
}
