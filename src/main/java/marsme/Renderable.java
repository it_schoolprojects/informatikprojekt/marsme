package marsme;

import javafx.scene.canvas.GraphicsContext;

/**
 * Definiert alle Objekte, die gerendert werden muessen
 *
 * @author Jan
 * @version 1.0.0
 */
public interface Renderable {
    /**
     * Fuehrt das Rendern aus
     *
     * @param context Graphical context of canvas
     */

    // This warning is a bug
    @SuppressWarnings("NullableProblems")
    void render(GraphicsContext context);

}
