package marsme;

/**
 * Definiert alle Objekte, die geupdated werden muessen
 *
 * @author Jan
 * @version 1.0.0
 */
public interface Updatable {
    /**
     * Fuehrt das update aus
     *
     * @param timeDiff Spielgeschwindigkeit
     */
    void update(double timeDiff);
}
