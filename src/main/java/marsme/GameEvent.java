package marsme;

/**
 * Klasse fuer das Repraesentieren eines Game Events
 *
 * @author Henrik
 * @version 1.0.0
 */

final class GameEvent {
    /**
     * Enthaelt den Event Typ
     * 1 = Idle
     * 2 = LowBattery
     * 3 = MetalOnSight
     * 4 = AlienOnSight
     * 5 = DataReceived
     *
     * @see LuaEventHandler
     */
    private final int eventType;

    /**
     * Daten die durch Rover zu Rover Kommunikation gewonnen werden
     */
    private String data;

    /**
     * Positionsdaten fuer den Ursprung des Events
     */
    private double x, y = -1;

    private GameEvent(int eventType, String data) {
        this.eventType = eventType;
        this.data = data;
    }

    private GameEvent(int eventType, double x, double y) {
        this.eventType = eventType;
        this.x = x;
        this.y = y;
    }

    private GameEvent(int eventType) {
        this.eventType = eventType;
    }

    static GameEvent generateIdle() {
        return new GameEvent(1);
    }

    static GameEvent generateMetalOnSight(double x, double y) {
        return new GameEvent(3, x, y);
    }

    static GameEvent generateAlienOnSight(double x, double y) {
        return new GameEvent(4, x, y);
    }

    static GameEvent generateLowBattery() {
        return new GameEvent(2);
    }

    static GameEvent generateDataReceived(String data) {
        return new GameEvent(5, data);
    }

    /**
     * EventType getter
     *
     * @return EventType
     */
    int getEventType() {
        return this.eventType;
    }

    /**
     * X getter
     *
     * @return x
     */
    double getX() {
        return this.x;
    }

    /**
     * Y getter
     *
     * @return y
     */
    double getY() {
        return this.y;
    }

    /**
     * Data getter
     *
     * @return data
     */
    String getData() {
        return this.data;
    }
}
