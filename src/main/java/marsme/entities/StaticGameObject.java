package marsme.entities;

import marsme.util.Vector2D;

public abstract class StaticGameObject extends GameObject {
    @SuppressWarnings("WeakerAccess")
    protected StaticGameObject(Vector2D aPosition, double aRot) {
        super(aPosition, aRot);
    }
}
