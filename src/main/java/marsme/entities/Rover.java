package marsme.entities;

import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;
import marsme.Assets;
import marsme.Game;
import marsme.util.Util;
import marsme.util.Vector2D;
import org.jetbrains.annotations.Nullable;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.JsePlatform;

import java.io.PrintStream;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Klasse fuer die Repraesentation des Zustandes eines einzelenen Rovers
 *
 * @author Jan, Henrik, Eric
 * @version 1.0.0
 */

@SuppressWarnings("unused")
public final class Rover extends DynamicGameObject {


    /**
     * Definiert die verbrauchte Batterieleistung
     */
    private static final double batteryLossPS = 20d / 9; // After 45 seconds battery empty
    /**
     * Definiert die Hoechstgeschwindigkeit
     */
    private static final double MAX_VELOCITY = 100;
    /**
     * Winkelgeschwindigkeit des Rovers
     */
    private static final double ROT_VELOCITY = 300;
    /**
     * Sichtweite des Rovers
     */
    private static final double VIEW_RANGE = 150;
    /**
     * Sichtwinkel des Rovers
     */
    private static final double VIEW_ANGLE = 100;

    private final Image img = new Image(Assets.ROVER_IMAGE.getResAsStream());

    private final int maxLoad;
    /**
     * Ermoeglicht dem Rover, Lua Code auszufuehren
     */
    private final LuaVM luaVM;
    /**
     * Postausgang des Rovers fuer die Interaktion zwischen den Rovern.
     */
    private final Queue<String> communicationOutbox = new ConcurrentLinkedQueue<>();
    /**
     * Anfragen an die Gameklasse werden hier gespeichert und abgearbeitet.
     */
    private final Queue<ActionRequest> actionRequests = new ConcurrentLinkedQueue<>();
    /**
     * Threadsicherer Zaehler fuer geladene Metallhaufen
     */
    private volatile int load;
    /**
     * Threadsicherer Batteriezustand
     */
    private volatile double battery = 100;
    /**
     * Threadsicherer Ortsvektor des aktuellen Ziels
     */

    @Nullable
    private volatile Vector2D target = null;
    /**
     * Gibt an, ob der Rover in diesem Augenblick minen kann.
     */
    private volatile boolean canMine = false;

    /**
     * Initialisiert den Rover
     *
     * @param rot            Anfaengliche Rotation des Rovers
     * @param initCode       LuaCode des Spielers
     * @param garagePosition Position der Garage
     * @param console        Konsole
     * @param maxLoad        Maximale Ladung
     */
    private Rover(Vector2D position, double rot, String initCode, Vector2D garagePosition, int maxLoad, PrintStream console) {
        super(position, rot);
        load = 0;
        RoverInterface roverInterface = new RoverInterface(garagePosition);
        this.luaVM = new LuaVM(initCode, roverInterface, console);
        this.maxLoad = maxLoad;
    }

    /**
     * Liefert die Sichtweite des Rovers
     *
     * @return s.o.
     */
    public static double getViewRange() {
        return VIEW_RANGE;
    }

    /**
     * Liefert den Sichtwinkel des Rovers
     *
     * @return s.o.
     */
    public static double getViewAngle() {
        return VIEW_ANGLE;
    }

    /**
     * Factory-Methode. Findet die Mitte heraus und spawnt dort einen Rover.
     *
     * @param initCode       LuaCode des Spielers
     * @param garagePosition Position der Garage
     * @param console        Konsole
     * @param maxLoad        Maximale Tragleistung des Roves
     * @return Zentralisierter Rover
     */
    public static Rover createCentralizedRover(String initCode, Vector2D garagePosition, int maxLoad, PrintStream console) {
        Rover rover = new Rover(Vector2D.nullVector(), 0, initCode, garagePosition, maxLoad, console);
        Rectangle2D screen = Game.getScreenBounds();
        rover.modCenterPosition(new Vector2D(screen.getWidth() / 2, screen.getHeight() / 2));
        return rover;
    }

    /**
     * Threadsicherers erhoehen der geladenene Metallhaufen
     */
    public synchronized void mineOre() {
        load++;
    }

    /**
     * Liefert, wie viele Metalheaps der Rover geladen hat
     *
     * @return s.o.
     */
    public int getLoad() {
        return load;
    }

    public int getMaxLoad() {
        return maxLoad;
    }

    /**
     * @return s.o.
     * @see marsme.entities.Rover#actionRequests
     */
    public Queue<ActionRequest> getActionRequests() {
        return actionRequests;
    }

    /**
     * Setzt die Anzahl geladener Metallhaufen auf null
     */
    public void resetLoad() {
        load = 0;
    }

    /**
     * Wiederaufladen der Batterie
     */
    public void recharge() {
        battery = 100;
    }

    @Override
    public Shape bounds() {
        return rectangularBounds();
    }

    @Override
    public Rectangle rectangularBounds() {
        Vector2D pos = getPosition();
        Rectangle bounds = new Rectangle(pos.getX(), pos.getY(), img.getWidth(), img.getHeight());
        bounds.setRotate(getRotation());
        return bounds;
    }

    /**
     * Ueberprueft ob das GameObject mit den Grenzen der Welt kollidiert ist und loest das Problem
     *
     * @return Wahr, wenn GameObject mit den Grenzen der Welt kollidiert, sonst falsch
     */
    @Override
    public boolean checkBorderCollision() {
        boolean val = super.checkBorderCollision();
        if (val) {
            target = null;
        }
        return val;
    }

    /**
     * Setzt das Ziel des Rovers, zu dem er gehen wird.
     *
     * @param x X-Koordinate des neuen Ziels
     * @param y Y-Koordinate des neuen Ziels
     */
    private void setTarget(double x, double y) {
        setTarget(new Vector2D(x, y));
    }

    /**
     * Liefert wahr wenn Rover MetalHeaps traegt
     *
     * @return s.o.
     */
    public boolean isLoaded() {
        return load > 0;
    }

    /**
     * Liefert das Ziel des Rovers
     *
     * @return Ziel des Rovers als Vector
     */
    @Nullable
    public Vector2D getTarget() {
        return target;
    }

    /**
     * Setzt das Ziel des Rovers, zu dem er gehen wird.
     *
     * @param target Neues Ziel
     */
    private void setTarget(@Nullable Vector2D target) {
        this.target = target;
    }

    /**
     * Zufaelliges durch die Gegend laufen
     */
    private synchronized void search() {
        if (target != null) {
            return;
        }

        Vector2D newTarget = new Vector2D(Util.random(0, Game.getScreenBounds().getWidth()), Util.random(0, Game.getScreenBounds().getHeight()));
        setTarget(newTarget);
    }

    /**
     * Liefert die Restbatterieladung in Prozent
     *
     * @return s.o.
     */
    public double getBattery() {
        return battery;
    }

    /**
     * Liefert die LuaVM
     *
     * @return s.o.
     */
    public LuaVM getLuaVM() {
        return luaVM;
    }

    /**
     * Liefert den Postausgang des Rovers
     *
     * @return s.o.
     */
    public Queue<String> getCommunicationOutbox() {
        return communicationOutbox;
    }

    public void setCanMine(boolean canMine) {
        this.canMine = canMine;
    }

    @Override
    public void render(GraphicsContext gc) {
        Vector2D pos = getPosition();
        gc.save();
        Rotate r = new Rotate(getRotation(), center().getX(), center().getY());
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
        gc.drawImage(new Image(Assets.ROVER_IMAGE.getResAsStream()), pos.getX(), pos.getY());
        gc.restore();
    }

    /**
     * Fuehrt das update aus
     */
    @SuppressWarnings("Duplicates")
    @Override
    public synchronized void update(double timeDiff) {
        // Anweisungen
        battery -= Util.amountPerTick(batteryLossPS, timeDiff);
        if (battery <= 0) {
            killEntity();
        }

        if (target != null) {
            Vector2D roverTargetVec = Vector2D.subtraction(Objects.requireNonNull(target), center());

            double targetRotation = Util.angle360(roverTargetVec);
            double rotation = Util.gradualRotation(getRotation(), targetRotation, ROT_VELOCITY, timeDiff);
            setRotation(rotation);
            if (roverTargetVec.getLength() < Util.amountPerTick(MAX_VELOCITY, timeDiff)) {
                modCenterPosition(Objects.requireNonNull(target));
                target = null;
            } else {
                setPosition(Util.gradualTranslation(getPosition(), MAX_VELOCITY, rotation, timeDiff));
            }
        }
    }

    /**
     * Actionrequests, die der Rover versenden kann
     */
    public enum ActionRequest {
        MINE()
    }

    /**
     * Verantwortlich fuer die Ausfuehrung des Lua Codes
     */
    public static final class LuaVM {
        /**
         * Enhaehlt die Datenstruktur welche die globalen Variablen der LuaVM enthaelt
         */
        private final Globals globals = JsePlatform.standardGlobals();

        private LuaVM(String initCode, RoverInterface roverInterface, PrintStream console) {
            globals.STDOUT = console;
            // Sperrt die User API in Lua ein

            LuaValue UserInterface = CoerceJavaToLua.coerce(roverInterface);
            this.globals.set("rover", UserInterface);
            this.globals.load(initCode).call();
        }

        public Globals getGlobals() {
            return this.globals;
        }
    }

    /**
     * Wird dem Spieler uebergeben. Mit diesem kann er Dinge mit dem Rover anstellen
     */
    @SuppressWarnings("WeakerAccess")
    public final class RoverInterface {
        private final Vector2D garagePosition;

        private RoverInterface(Vector2D garagePosition) {
            this.garagePosition = garagePosition;
        }

        /**
         * Moves the rover back to garage.
         */
        public void moveToGarage() {
            double[] garagePos = getGaragePosition();
            moveTo(garagePos[0], garagePos[1]);
        }

        /**
         * Liefert die Koordinaten der Garage als (X | Y)
         *
         * @return Koordinaten als (X | Y)
         */

        public double[] getGaragePosition() {
            return garagePosition.asArray();
        }

        /**
         * Graphically moves the rover
         *
         * @param x X Position to move to
         * @param y Y Position to move to
         */
        public void moveTo(double x, double y) {
            Rover.this.setTarget(x, y);
        }

        /**
         * Checks if rover is on ore field
         *
         * @return if rover is on ore field
         */
        public boolean isOnOreField() {
            return Rover.this.canMine;
        }

        /**
         * Mines the ore.
         */
        public void mine() {
            Rover.this.actionRequests.add(ActionRequest.MINE);
        }

        /**
         * Liefert Center des Rovers
         *
         * @return s.o.
         */
        public double[] getPosition() {
            return Rover.this.center().asArray();
        }

        /**
         * Methode um zu ueberpruefen ob der Rover geladen ist
         *
         * @return siehe Beschreibung
         */
        public boolean isLoaded() {
            return Rover.this.getLoad() > 0;
        }

        /**
         * Bewegt den Rover an eine zufaellige Position auf dem Spielfeld.
         */
        public void search() {
            Rover.this.search();
        }

        /**
         * Methode um anderen Rovern Informationen zu uebermitteln
         *
         * @param data da diese Methode aus Lua aufgerufen wird nimmt sie eine LuaTable als Parameter (Java Leute wuerden sie als HashMap bezeichnen)
         */
        public void communicate(String data) {
            Rover.this.getCommunicationOutbox().add(data);
        }

        /**
         * Liefert maximale Tragfaehigkeit des Rovers
         *
         * @return s.o.
         */
        public int getMaxLoad() {
            return Rover.this.maxLoad;
        }

        /**
         * Liefert aktuelle Batterierestladung des Rovers
         *
         * @return s.o.
         */
        public double getBattery() {
            return Rover.this.battery;
        }

        /**
         * Liefert das aktuelle Ziel des Rovers
         *
         * @return Aktueller Zielvektor des Rovers als geordnetes (x, y)-Tupel.
         */
        @Nullable
        public double[] getTarget() {
            synchronized (Rover.this) {
                return Rover.this.target != null ? Objects.requireNonNull(Rover.this.target).asArray() : null;
            }
        }
    }

}
