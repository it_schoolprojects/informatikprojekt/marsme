package marsme.entities;

import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import marsme.Assets;
import marsme.Game;
import marsme.util.Vector2D;


/**
 * Repraesentiert die Rover Garage
 *
 * @author Jan
 * @version 1.0.0
 */
public final class Garage extends StaticGameObject {

    private final Image img = new Image(Assets.GARAGE_IMAGE.getResAsStream());

    /**
     * Die Anzahl an Metall welches sich gerade in der Garage befindet
     */
    private volatile int fIronNum;

    private Garage(Vector2D aPosition) {
        super(aPosition, 0);
    }

    public static Garage createCentralizedGarage() {
        Garage garage = new Garage(Vector2D.nullVector());
        Rectangle2D screen = Game.getScreenBounds();
        garage.modCenterPosition(new Vector2D(screen.getWidth() / 2, screen.getHeight() / 2));
        return garage;
    }

    @Override
    public void render(GraphicsContext gc) {
        Vector2D position = getPosition();
        gc.drawImage(new Image(Assets.GARAGE_IMAGE.getResAsStream()), position.getX(), position.getY());
    }

    @Override
    public Shape bounds() {
        return rectangularBounds();
    }

    @Override
    public Rectangle rectangularBounds() {
        Vector2D pos = getPosition();
        return new Rectangle(pos.getX(), pos.getY(), img.getWidth(), img.getHeight());
    }

    public int getIronNum() {
        return fIronNum;
    }

    /**
     * Threadsicheres hinzufuegen von Metall
     *
     * @param aNum Wie viel Metall hinzugefuegt werden soll
     */
    public synchronized void addIronNum(int aNum) {
        fIronNum += aNum;
    }

    public synchronized void remIronNum(int aNum) {
        fIronNum -= aNum;
    }

}
