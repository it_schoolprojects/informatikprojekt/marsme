package marsme.entities;

import javafx.geometry.Bounds;
import javafx.geometry.Rectangle2D;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import marsme.Game;
import marsme.Renderable;
import marsme.util.Vector2D;

/**
 * Alle GameObjects erben von dieser Klasse.
 *
 * @author Jan, Henrik
 * @version 1.0.0
 */
public abstract class GameObject implements Renderable {

    private volatile Vector2D fPosition;
    private volatile double fRot;
    private volatile boolean fAlive = true;

    @SuppressWarnings("WeakerAccess")
    protected GameObject(Vector2D aPosition, double aRot) {
        fPosition = aPosition;
        fRot = aRot;
    }

    public static boolean isCollision(GameObject g1, GameObject g2) {
        Bounds intersection = Shape.intersect(g1.bounds(), g2.bounds()).getBoundsInLocal();
        return intersection.getWidth() > 0 && intersection.getHeight() > 0;
    }

    public abstract Shape bounds();

    public abstract Rectangle rectangularBounds();

    public Vector2D center() {
        Rectangle bounds = rectangularBounds();
        return new Vector2D(bounds.getX() + bounds.getWidth() / 2, bounds.getY() + bounds.getHeight() / 2);
    }

    public boolean checkBorderCollision() {
        boolean collision = false;
        Bounds bounds = bounds().getBoundsInParent();
        Rectangle2D screen = Game.getScreenBounds();
        double deltaX = 0;
        double deltaY = 0;
        if (bounds.getMinX() < 0) {
            deltaX = -bounds.getMinX();
            collision = true;
        } else if (bounds.getMaxX() > screen.getMaxX()) {
            deltaX = screen.getMaxX() - bounds.getMaxX();
            collision = true;
        }
        if (bounds.getMinY() < 0) {
            deltaY = -bounds.getMinY();
            collision = true;
        } else if (bounds.getMaxY() > screen.getMaxY()) {
            deltaY = screen.getMaxY() - bounds.getMaxY();
            collision = true;
        }
        if (collision) {
            synchronized (this) {
                fPosition = Vector2D.addition(fPosition, new Vector2D(deltaX, deltaY));
            }
        }
        return collision;
    }

    @SuppressWarnings("WeakerAccess")
    protected final void modCenterPosition(Vector2D aPosition) {
        Vector2D distance = Vector2D.subtraction(aPosition, center());
        setPosition(Vector2D.addition(fPosition, distance));
    }

    @SuppressWarnings("WeakerAccess")
    public final Vector2D getPosition() {
        return fPosition;
    }

    @SuppressWarnings("WeakerAccess")
    protected final void setPosition(Vector2D aPosition) {
        fPosition = aPosition;
    }

    public final double getRotation() {
        return fRot;
    }

    @SuppressWarnings("WeakerAccess")
    protected final void setRotation(double aRot) {
        if (aRot > 0) {
            fRot = aRot % 360;
        } else {
            fRot = 360 + aRot % 360;
        }
    }

    public final boolean isAlive() {
        return fAlive;
    }

    public final boolean isDead() {
        return !fAlive;
    }

    public final void killEntity() {
        fAlive = false;
    }


}
