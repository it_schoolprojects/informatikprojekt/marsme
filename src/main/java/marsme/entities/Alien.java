package marsme.entities;

import javafx.geometry.Bounds;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;
import marsme.Assets;
import marsme.util.Util;
import marsme.util.Vector2D;

/**
 * Repraesentiert die Aliens
 *
 * @author Jan, Henrik
 * @version 1.0.0
 */
public final class Alien extends DynamicGameObject {

    /**
     * Maximal Geschwindigkeit des Aliens
     */
    private static final double MAX_VELOCITY = 50;

    /**
     * Maximal Rotationsgeschwindigkeit
     */
    private static final double ROT_VELOCITY = 90;
    /**
     * Alien-Grafik
     */
    private final Image img = new Image(Assets.ALIEN_IMAGE.getResAsStream());

    private double timer = 0;
    /**
     * Aktuelle Rotation
     */
    private double targetRotation = 0;

    /**
     * Initialisiert ein Alien
     *
     * @param position Position des Aliens
     */
    private Alien(Vector2D position) {
        super(position, 0);
    }

    /**
     * Factory-Methode. Liefert einen lokalisierten Alien
     *
     * @param position Position des Aliens
     * @return s.o.
     */
    public static Alien constructLocatedAlien(Vector2D position) {
        return new Alien(position);
    }

    @Override
    public void render(GraphicsContext gc) {
        Vector2D pos = getPosition();
        Vector2D center = center();

        gc.save();
        Rotate r = new Rotate(getRotation(), center.getX(), center.getY());
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
        gc.drawImage(img, pos.getX(), pos.getY());
        gc.restore();
    }

    /**
     * Fuehrt das update aus
     */
    @Override
    public synchronized void update(double timeDiff) {
        timer += timeDiff;
        double rotation = getRotation();
        if (timer >= 0.5) {
            timer -= 0.5;
            double deltaTargetRotation = (Util.random(-20, 20));
            this.targetRotation = (rotation + deltaTargetRotation) % 360;
        }

        double newRotation = Util.gradualRotation(rotation, targetRotation, ROT_VELOCITY, timeDiff);
        setRotation(newRotation);
        setPosition(Util.gradualTranslation(getPosition(), MAX_VELOCITY, newRotation, timeDiff));
    }

    @Override
    public Shape bounds() {
        Rectangle rect = rectangularBounds();
        Vector2D center = center();
        Ellipse bounds = new Ellipse(center.getX(), center.getY(), rect.getWidth() / 2, rect.getHeight() / 2);
        bounds.setRotate(getRotation());
        return bounds;
    }

    @Override
    public Rectangle rectangularBounds() {
        Vector2D pos = getPosition();
        Rectangle bounds = new Rectangle(pos.getX(), pos.getY(), img.getWidth(), img.getHeight());
        bounds.setRotate(getRotation());
        return bounds;
    }

    /**
     * Ueberprueft ob das GameObject mit den Grenzen der Welt kollidiert ist und loest das Problem.
     * Weiterhin wird die Laufrichtung des Aliens invertiert.
     *
     * @return Wahr, wenn GameObject mit den Grenzen der Welt kollidiert, sonst falsch
     */
    @Override
    public boolean checkBorderCollision() {
        boolean val = super.checkBorderCollision();
        if (val) {
            synchronized (this) {
                this.targetRotation = getRotation() + 180;
                setRotation(targetRotation);
            }
        }
        return val;
    }

    /**
     * Ueberprueft ob eine Kollision mit der Garage aufgetreten ist
     *
     * @param garage Garage
     */
    public void resolveGarageCollision(Garage garage) {
        Bounds intersection = Shape.intersect(bounds(), garage.bounds()).getBoundsInParent();
        Bounds garageBounds = garage.bounds().getBoundsInParent();
        double deltaX = 0;
        double deltaY = 0;
        if (intersection.getMaxX() > garageBounds.getMinX() && Util.doubleEquals(intersection.getMinX(), garageBounds.getMinX())) {
            deltaX = garageBounds.getMinX() - intersection.getMaxX();
        } else if (intersection.getMinX() < garageBounds.getMaxX() && Util.doubleEquals(intersection.getMaxX(), garageBounds.getMaxX())) {
            deltaX = garageBounds.getMaxX() - intersection.getMinX();
        }
        if (intersection.getMaxY() > garageBounds.getMinY() && Util.doubleEquals(intersection.getMinY(), garageBounds.getMinY())) {
            deltaY = garageBounds.getMinY() - intersection.getMaxY();
        } else if (intersection.getMinY() < garageBounds.getMaxY() && Util.doubleEquals(intersection.getMaxY(), garageBounds.getMaxY())) {
            deltaY = garageBounds.getMaxY() - intersection.getMinY();
        }
        synchronized (this) {
            setPosition(Vector2D.addition(getPosition(), new Vector2D(deltaX, deltaY)));
        }
    }
}
