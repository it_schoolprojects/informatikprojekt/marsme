package marsme.entities;


import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Speichert alle GameObjects
 *
 * @author Jan
 */
public final class EntityManager {

    private final CopyOnWriteArrayList<Garage> fGarages = new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<MetalHeap> fHeaps = new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<Rover> fRovers = new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<Alien> fAliens = new CopyOnWriteArrayList<>();

    private final CopyOnWriteArrayList<CopyOnWriteArrayList<? extends StaticGameObject>> fStatics = new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<CopyOnWriteArrayList<? extends DynamicGameObject>> fDynamics = new CopyOnWriteArrayList<>();

    private final CopyOnWriteArrayList<CopyOnWriteArrayList<? extends GameObject>> fAll = new CopyOnWriteArrayList<>();

    // Initializer block
    {
        fStatics.add(fGarages);
        fDynamics.add(fRovers);
        fDynamics.add(fAliens);
        fDynamics.add(fHeaps);

        fAll.addAll(fDynamics);
        fAll.addAll(fStatics);
    }

    public List<? extends List<? extends DynamicGameObject>> getDynamics() {
        return fDynamics;
    }

    public List<? extends List<? extends StaticGameObject>> getStatics() {
        return fStatics;
    }

    public List<? extends List<? extends GameObject>> getAll() {
        return fAll;
    }

    public List<Alien> getAliens() {
        return fAliens;
    }

    public List<Rover> getRovers() {
        return fRovers;
    }

    public List<Garage> getGarages() {
        return fGarages;
    }

    public List<MetalHeap> getHeaps() {
        return fHeaps;
    }

    public void clear() {
        fAll.forEach(List::clear);
    }

}
