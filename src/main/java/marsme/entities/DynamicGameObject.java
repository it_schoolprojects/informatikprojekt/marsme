package marsme.entities;

import marsme.Updatable;
import marsme.util.Vector2D;

public abstract class DynamicGameObject extends GameObject implements Updatable {

    @SuppressWarnings("WeakerAccess")
    protected DynamicGameObject(Vector2D aPosition, double aRot) {
        super(aPosition, aRot);
    }
}
