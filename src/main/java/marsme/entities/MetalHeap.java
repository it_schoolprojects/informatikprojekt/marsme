package marsme.entities;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import marsme.Assets;
import marsme.util.Vector2D;

/**
 * Repraesentiert die Metallhaufen im Spiel
 *
 * @author Henrik
 * @version 1.0.0
 */
public final class MetalHeap extends DynamicGameObject {

    private final Image img = new Image(Assets.METAL_IMAGE.getResAsStream());

    /**
     * Konstruktor der MetalHeaps
     *
     * @param position Position des Metalls
     */
    private MetalHeap(Vector2D position) {
        super(position, 0);
    }

    /**
     * Static-Factory-Method. Konstruiert einen lokalisierten Alien.
     *
     * @return s.o.
     */
    public static MetalHeap constructLocatedMetalHeap(Vector2D position) {
        return new MetalHeap(position);
    }

    @Override
    public void render(GraphicsContext gc) {
        Vector2D pos = getPosition();
        gc.drawImage(img, pos.getX(), pos.getY());
    }

    @Override
    public Shape bounds() {
        return rectangularBounds();
    }

    @Override
    public Rectangle rectangularBounds() {
        Vector2D pos = getPosition();
        return new Rectangle(pos.getX(), pos.getY(), img.getWidth(), img.getHeight());
    }

    @Override
    public void update(double timeDiff) {

    }
}
