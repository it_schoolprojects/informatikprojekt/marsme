package marsme.gui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import marsme.Assets;
import marsme.Game;
import marsme.editor.Editor;
import marsme.util.IconManager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Klasse verantwortlich fuer das Steuern des Start-Fensters
 *
 * @author Eric, Jan, Henrik
 * @version 1.0.0
 */

public final class MainController implements Initializable {
    /**
     * Titel des Start-Windows, der ueber ein TextFlow angezeigt wird
     */
    private final Text title = new Text("MarsMe!");

    /**
     * Der Editor
     */
    private final Editor editor = new Editor(this);
    /**
     * Die Anzahl der Alien, Rover, benoetigten Metalle und max. Rover Ladung in der Reihenfolge bei den einzelnen Schwierigkeitsstufen
     */
    private final int[][] difficulties = {{6, 2, 5, 2}, {10, 2, 5, 2}, {10, 1, 5, 1}, {10, 1, 10, 1}};
    /**
     * Die verschiedenen Schnellgkeiten bei der Simulation
     */
    private final int[] speeds = {1, 2, 3, 4};
    /**
     * Der vom Benutzer geschriebene Luacode
     */
    private String saveCode;
    /**
     * Dieses Array enthaelt alle Informationen einer Schwierigkeitsstufe in folgender Reihenfolge:
     * Anzahl der Aliens, Anzahl der Rover, Anzahl der benoetigten Metallhaufen, um einen neuen
     * Marsrover zu erhalten.
     */
    private int[] difficulty = difficulties[0];
    /**
     * Die Schnelligkeit die ausgewaehlt wurde. Default ist 1.
     */
    private int speed = speeds[0];

    /**
     * Referenzattribute fuer graphische Komponenten
     */
    @FXML
    private VBox mainContainer;
    @FXML
    private MenuButton settingsMenu;
    @FXML
    private TextFlow textFlow;
    @FXML
    private ToggleGroup difficulty_toggleGroup;
    @FXML
    private ToggleGroup speed_toggleGroup;

    /**
     * Initialisiert den Controller nachdem das Fenster komplett erstellt wurde.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        textFlow.getChildren().add(title);
        settingsMenu.setGraphic(new ImageView(new Image(Assets.SETTIGNS_IMAGE.getResAsStream())));

        difficulty_toggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                switch (newValue.getUserData().toString()) {
                    case "s":
                        difficulty = difficulties[0];
                        break;
                    case "m":
                        difficulty = difficulties[1];
                        break;
                    case "d":
                        difficulty = difficulties[2];
                        break;
                    case "e":
                        difficulty = difficulties[3];
                        break;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        });
        speed_toggleGroup.selectedToggleProperty().addListener(((observable, oldValue, newValue) -> {
            if (newValue != null) {
                switch (newValue.getUserData().toString()) {
                    case "1":
                        speed = speeds[0];
                        break;
                    case "2":
                        speed = speeds[1];
                        break;
                    case "3":
                        speed = speeds[2];
                        break;
                    case "4":
                        speed = speeds[3];
                        break;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }));

    }

    /**
     * Diese Methode wird aufgerufen, wenn das MenuItem mit der Aufschrift 'Help' angeklickt wird.
     * <p>
     * Es wird ein pop-up window geoeffnet, auf dem das Spiel erklaert wird.
     *
     * @throws IOException Wenn die fxml-Ressource nicht gefunden werden kann
     */
    @FXML
    private void onClick_helpMenuItem() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fx/gameHelp.fxml"));
        final Parent root = loader.load();
        final Scene scene = new Scene(root);
        Stage helpStage = new Stage();
        helpStage.setScene(scene);
        helpStage.setResizable(false);
        helpStage.initStyle(StageStyle.UNDECORATED);
        helpStage.getIcons().addAll(IconManager.mainIcons);
        helpStage.show();

        GameHelpController controller = loader.getController();
        controller.setOnClose(() -> {
            ((Stage) mainContainer.getScene().getWindow()).show();
            init();
        });
        controller.init();

        mainContainer.getScene().getWindow().hide();
    }

    /**
     * Diese Methode wird aufgerufen, wenn der Button mit der Aufschrift 'Start Simulation' angeklickt wird.
     * <p>
     * Es wird die Simulation gestartet und das Start-Fenster ist nicht mehr sichtbar. Er wird allerdings nicht
     * geschlossen um Resourcen zu sparen.
     */
    @FXML
    private void onClick_simulateButton() {
        editor.getCode();
        startSimulation();
        mainContainer.getScene().getWindow().hide();
    }

    /**
     * Diese Methode wird aufgerufen, wenn der Button mit der Aufschrift 'Open Rover Source Code' angeklickt
     * wird.
     * <p>
     * Es wird der Editor geoeffnet, um Aenderungen am Verhalten der Rover vorzunehmen.
     */
    @FXML
    private void onClick_editButton() {
        this.editor.textField.setText(this.saveCode);
        this.editor.repaint();
        this.editor.setVisible(true);
        this.editor.toFront();
    }

    /**
     * Diese Methode wir aufgerufen, wenn der Button mit der Aufschrift 'Exit' angeklickt wird.
     * <p>
     * Da das Fenster nicht ueber den standardmaessig vorhandene 'Close'-Button in der rechten oberen
     * Ecke eines Fensters geschlossen werden kann, uebernimmt dieser Button diese Funktion.
     */
    @FXML
    private void onClick_exitButton() {
        Platform.exit();
        System.exit(0);
    }

    /**
     * Diese Methode erzeugt ein neues Objekt der Game-Klasse und startet die Simulation, d.h. die Game-Loop.
     */
    private void startSimulation() {
        String initCode = this.editor.getCode();

        Runnable onStop = () -> {
            ((Stage) mainContainer.getScene().getWindow()).show();
            init();
        };
        final Game game = new Game(initCode, onStop, difficulty[0], difficulty[1], difficulty[2], difficulty[3], speed);
        game.start();
    }

    /**
     * Legt den Fokus auf das Fenster
     */
    public void init() {
        mainContainer.requestFocus();
    }

    /**
     * Setzt den Luacode.
     *
     * @param code s.o.
     * @see #saveCode
     */
    public void setSaveCode(String code) {
        saveCode = code;
    }
}
