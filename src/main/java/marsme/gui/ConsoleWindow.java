package marsme.gui;

import marsme.util.IconManager;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public final class ConsoleWindow extends JFrame {

    private final TextArea console = new TextArea();

    // Initializer block
    {
        console.setEditable(false);
        console.setSize(600, 500);
        add(console);
        pack();
        setTitle("Console");
        setIconImages(Arrays.asList(IconManager.consoleIcons));
    }

    public void print(String s) {
        console.append(s);
        console.setCaretPosition(console.toString().length());
    }

    @Override
    public void requestFocus() {
        console.requestFocus();
    }
}
