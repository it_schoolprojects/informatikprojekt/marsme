package marsme.gui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.jetbrains.annotations.Nullable;

public final class GameHelpController {

    @FXML
    private VBox mainContainer;

    private Runnable onClose;

    void setOnClose(@Nullable Runnable onClose) {
        this.onClose = (onClose == null ? () -> {
        } : onClose);
    }

    @FXML
    private void onClick_exitButton() {
        onClose.run();
        ((Stage) mainContainer.getScene().getWindow()).close();
    }

    void init() {
        mainContainer.requestFocus();
    }

}
