package marsme.datastructures.queue;

/**
 * Wird ausgeloest, wenn versucht wird, ein Element von einer bereits leeren Warteschlange zu entfernen.
 *
 * @author Victor
 * @version 1.0.0
 */

public final class QUnderflowException extends RuntimeException {
}
