package marsme.datastructures.queue;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.Objects;

/**
 * Warteschlange mit fester Anzahl.
 *
 * @param <E> Zu speichernder Datentyp
 * @author Victor
 * @version 1.0.0
 */
public final class Queue<E> implements Iterable<E> {

    /**
     * Dieses Feld speichert alle Elemente
     */
    private final E[] queue;
    /**
     * Dieses Feld sagt, welche Elemente uebeschrieben werden duerfen (true)
     * bzw. welche zuerst noch gelesen werden muessen (false)
     */
    private final boolean[] writable;
    /**
     * Groessee der Warteschlange
     */
    private final int maxSize;
    /**
     * Dieser Index wird als naechstes gelesen
     */
    private volatile int readCounter = 0;
    /**
     * Dieser Index wird als naechstes beschrieben
     */
    private volatile int writeCounter = 0;

    /**
     * Erstellt eine neue Warteschlange.
     *
     * @param maxSize Groesse der Warteschlange
     * @param eClass  Zu speichernder Typ
     */
    @SuppressWarnings("unchecked")
    public Queue(int maxSize, Class<E> eClass) {
        if (maxSize <= 0) {
            throw new IllegalArgumentException("Size can't be 0 or less");
        }
        //Creates Array with Datatyp E and size maxSize
        final E[] queue = (E[]) Array.newInstance(eClass, maxSize);
        final boolean[] writable = new boolean[maxSize];
        for (int i = 0; i < maxSize; i++) {
            writable[i] = true;
        }
        this.queue = queue;
        this.writable = writable;
        this.maxSize = maxSize;
    }

    /**
     * Haengt ein Element ans Ende der Warteschlange an, sofern sie noch nicht voll ist.
     *
     * @param element Zu hinzufuegendes Element (!= null)
     */
    public void add(E element) {
        Objects.requireNonNull(element);
        if (writable[writeCounter]) {
            //Overrides last read element
            queue[writeCounter] = element;
            //Mark index as to read
            writable[writeCounter] = false;
            synchronized (this) {
                //Increase writeCounter and reset to 0 if it reaches maxSize
                writeCounter = (writeCounter + 1) % maxSize;
            }
        } else {
            throw new QOverflowException();
        }
    }

    /**
     * Liest das naechste Element und loescht dieses
     *
     * @return Naechste Element in der Warteschlange
     */
    public E read() {
        if (!writable[readCounter]) {
            //Fetch next element
            final E element = queue[readCounter];
            //Mark index as read and writable
            writable[readCounter] = true;
            synchronized (this) {
                //Increase readCounter and reset to 0 if it reaches maxSize
                readCounter = (readCounter + 1) % maxSize;
            }
            return element;
        }
        throw new QUnderflowException();
    }

    public void clear() {
        for (int i = 0; i < maxSize; i++) {
            writable[i] = true;
        }
    }

    /**
     * Liefert wahr, wenn Warteschlange nicht leer ist
     *
     * @return Ist Warteschlange nicht leer
     */
    public final boolean hasNext() {
        return !writable[readCounter];
    }

    /**
     * Liefert wahr, wenn Warteschlange voll ist
     *
     * @return Ist Warteschlange voll
     */
    public final boolean isFull() {
        return !writable[writeCounter];
    }

    public final boolean isEmpty() {
        return !hasNext();
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            @Override
            public boolean hasNext() {
                return Queue.this.hasNext();
            }

            @Override
            public E next() {
                return read();
            }
        };
    }
}
