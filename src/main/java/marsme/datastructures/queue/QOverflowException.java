package marsme.datastructures.queue;

/**
 * Wird ausgeloest, wenn versucht wird, ein Element zu einer bereits vollen Warteschlange hinzuzufuegen.
 *
 * @author Victor
 * @version 1.0.0
 */

public final class QOverflowException extends RuntimeException {
}
