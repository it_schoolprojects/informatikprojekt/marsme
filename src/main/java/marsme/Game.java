package marsme;

import javafx.animation.AnimationTimer;
import javafx.geometry.Bounds;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import marsme.entities.*;
import marsme.entities.Rover.LuaVM;
import marsme.util.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Diese Klasse initialisiert das Spielfenster und sorgt fuer die GameLoop.
 *
 * @author Jan, Henrik, Eric
 * @version 1.0.0
 */
public final class Game {
    /**
     * Diese Attribute enthalten die benoetigten Informationen ueber den Screen
     */
    private static final Rectangle2D screen;
    private static final double screenWidth;
    private static final double screenHeight;

    static {
        screen = Screen.getPrimary().getVisualBounds();
        screenWidth = screen.getWidth();
        screenHeight = screen.getHeight();
    }

    /**
     * Dieses Objekt verwaltet die Eigenschaften des GameWindows
     */
    private final Stage stage;

    private final GraphicsContext dynamicGC;

    private final GraphicsContext staticGC;

    /**
     * Grafikcontainer, oberste Instanz in der Grafikordnung
     */
    private final Scene scene;
    /**
     * Hauptgruppe von grafischen Elementen
     */
    private final Group root;
    /**
     * Verantwortlich fuer das speichern von Aliens, Rovern und Metallhaufen
     */
    private final EntityManager entityManager;
    /**
     * Intialer Code fuer die LuaVMs
     *
     * @see LuaVM
     */
    private final String initCode;
    /**
     * Gibt an, wie viel Metall benoetigt wird, um einen neuen Rover zu bauen
     */
    private final int numNeededMetal;
    /**
     * Gibt die maximale Traglast des Rovers an
     */
    private final int roverMaxLoad;
    /**
     * Gibt die Spielgeschwindigkeit an
     */
    private final double speed;
    private final HookedPrintStream console;
    private final Label elapsedTime = new Label();
    private final Label scoreLabel = new Label();
    private final Label metalLabel = new Label();
    private final Label roverCounter = new Label();

    /**
     * Diese Variable gibt an, ob die GameLoop abbrechen soll oder nicht
     */
    private volatile boolean running = false;
    /**
     * Die Anzahl der insgesamt zu Garage gebrachten Metallhaufen
     */
    private volatile int score = 0;
    private volatile int fps = 0;

    /**
     * Konstruktor der Game Klasse.
     *
     * @param initCode       siehe initCode Attribut
     * @param onClose        die auszufuehrende Methode beim Schliessen des Fensters
     * @param numAlien       Anzahl der Aliens
     * @param numRover       Anzahl der Rover am Anfang
     * @param numNeededMetal Anzahl an Metall, um einen Rover zu bauen
     * @param roverMaxLoad   Maximale Tragleistung des Rovers
     * @param speed          Spielgeschwindigkeit
     */
    public Game(String initCode, Runnable onClose, int numAlien, int numRover, int numNeededMetal, int roverMaxLoad, double speed) {
        Canvas staticC = new Canvas(screenWidth, screenHeight);
        Canvas dynamicC = new Canvas(screenWidth, screenHeight);
        VBox information = new VBox();
        StackPane stack = new StackPane(staticC, dynamicC, information);

        this.console = new HookedPrintStream();
        this.initCode = initCode;
        this.numNeededMetal = numNeededMetal;
        this.roverMaxLoad = roverMaxLoad;
        this.stage = new Stage();
        this.root = new Group();
        this.scene = new Scene(root, Color.web("#e77d11"));

        this.staticGC = staticC.getGraphicsContext2D();
        this.dynamicGC = dynamicC.getGraphicsContext2D();
        this.speed = speed;
        this.entityManager = new EntityManager();

        information.getChildren().addAll(scoreLabel, metalLabel, roverCounter, elapsedTime);

        root.getChildren().add(stack);

        stage.setScene(scene);
        stage.setOnCloseRequest(event -> {
            System.out.println("SYSTEM: ##### Simulation terminated #####");
            stop();
            onClose.run();
            console.close();
        });

        stage.setTitle("MarsMe");
        stage.setMaximized(true);
        stage.setResizable(false);
        stage.getIcons().addAll(IconManager.mainIcons);

        init(numRover, numAlien, roverMaxLoad);
        System.setOut(console);
        System.setErr(console);
    }

    /**
     * Getter fuer das Screen Attribut
     *
     * @return Das Screen Attribut
     */
    public static Rectangle2D getScreenBounds() {
        return screen;
    }

    /**
     * Initialisiert die ersten Rovers und Aliens.
     *
     * @param rovers       Anzahl der zu spawnenden Rovers
     * @param aliens       Anzahl der zu spawnenden Aliens
     * @param roverMaxLoad Maximale Tragfaehigkeit des Rovers
     */
    private void init(int rovers, int aliens, int roverMaxLoad) {

        entityManager.getGarages().add(Garage.createCentralizedGarage());

        for (int i = 0; i < rovers; i++) {
            entityManager.getRovers().add(Rover.createCentralizedRover(initCode, getGarage().center(), roverMaxLoad, console));
        }

        for (int i = 0; i < aliens; i++) {
            Alien alien;
            do {
                alien = Alien.constructLocatedAlien(new Vector2D(Util.random(0, screen.getMaxX()), Util.random(0, screen.getMaxY())));
            } while (GameObject.isCollision(alien, getGarage()));
            entityManager.getAliens().add(alien);
        }

        for (List<? extends StaticGameObject> list : entityManager.getStatics()) {
            list.forEach(g -> g.render(staticGC));
        }
    }

    /**
     * Plaziert die Mitte des GameOver Screens in der Mitte der Scene
     *
     * @return GameOver Screen
     */
    private ImageView getCentralizedGameOverScreen() {
        ImageView val = new ImageView(new Image(Assets.GAMEOVER_SCREEN.getResAsStream()));
        Bounds imgBounds = val.getBoundsInLocal();

        double centralX = screenWidth / 2 - imgBounds.getWidth() / 2;
        double centralY = screenHeight / 2 - imgBounds.getHeight() / 2;

        val.setTranslateX(centralX);
        val.setTranslateY(centralY);
        val.setSmooth(true);
        val.setCache(true);

        return val;
    }

    /**
     * Startet das Game. Initialisiert alle Game relevanten Threads und AnimationTimer
     */
    public void start() {
        if (running) {
            throw new IllegalStateException("Game already running!");
        }

        running = true;
        stage.show();

        console.show();
        console.println("SYSTEM: ##### Simulation started #####");

        /*
        Zwingt die Framerate auf maximal 60 FPS.
        Sorgt fuer die Collision Detection.
        Loest Border Collisions (Kollision gegen den Rand der Spielwelt.
        Sorgt fuer das Aufladen von Rovern bei der Garage.
        Erhoeht den Score counter, falls der Rover Metall dabei hat.
        Findet heraus welche Rover auf welchen Metallhaufen stehen.
        Ist der GarbageCollector fuer tote Game Elemente.
         */
        AnimationTimer gameLoop = new AnimationTimer() {

            // Thread fuer die Collision Detection.
            private final Runnable collisionDetectionFunction = () -> {
                try {
                    for (Rover r : Game.this.entityManager.getRovers()) {
                        for (Alien a : Game.this.entityManager.getAliens()) {
                            if (GameObject.isCollision(r, a)) {
                                r.killEntity();
                                System.out.println("SYSTEM: Rover died!");
                            }
                        }
                    }
                    for (Alien a : Game.this.entityManager.getAliens()) {
                        if (GameObject.isCollision(a, Game.this.getGarage())) {
                            a.resolveGarageCollision(getGarage());
                        }
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            };

            // Loest Border Collisions.
            private final Runnable resolveBorderCollision = () -> {
                try {
                    for (List<? extends DynamicGameObject> list : Game.this.entityManager.getDynamics()) {
                        list.forEach(GameObject::checkBorderCollision);
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            };

            // Laedt die Batterie des Rovers auf und "laedt" Metall ab
            private final Runnable garageFunction = () -> {
                try {
                    for (Rover r : Game.this.entityManager.getRovers()) {
                        Garage garage = Game.this.getGarage();
                        if (r.center().equals(garage.center())) {
                            int load = r.getLoad();
                            garage.addIronNum(load);
                            r.resetLoad();
                            r.recharge();
                            int oldScore = Game.this.score;
                            synchronized (Game.this) {
                                Game.this.score += load;
                            }
                        }
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            };

            // Findet heraus welche Rover auf welchen Metallhaufen stehen
            private final Runnable metalHeapCollectionFunction = () -> {
                try {
                    for (Rover r : Game.this.entityManager.getRovers()) {
                        //noinspection SynchronizationOnLocalVariableOrMethodParameter
                        synchronized (r) {
                            r.setCanMine(false);
                            for (MetalHeap m : Game.this.entityManager.getHeaps()) {
                                if (GameObject.isCollision(r, m)) {
                                    r.setCanMine(true);
                                    break;
                                }
                            }
                        }
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            };

            /*
            Erlaubt es den Rovern untereinander zu kommunizieren
             */
            Runnable communicationDistribution = () -> {
                for (Rover r : Game.this.entityManager.getRovers()) {
                    synchronized (r.getCommunicationOutbox()) {
                        while (!r.getCommunicationOutbox().isEmpty()) {
                            String msg = r.getCommunicationOutbox().poll();
                            if (msg != null) {
                                for (Rover receiver : entityManager.getRovers()) {
                                    if (r != receiver) {
                                        LuaEventHandler.handleEvent(GameEvent.generateDataReceived(msg), receiver.getLuaVM());
                                    }
                                }
                            }
                        }
                    }
                }
            };

            /*
             * Berechnet alle Events, Default ist Idle
             */
            Runnable eventCalculator = () -> {
                for (Rover r : entityManager.getRovers()) {

                    // Ausgangsvektoren fuer das Sichtfeld des Rovers
                    Vector2D center = r.center();
                    Vector2D base = new Vector2D(Rover.getViewRange(), 0);
                    Vector2D startSector = Vector2D.generateRotatedVector(base, 360 - Rover.getViewAngle() / 2);
                    Vector2D endSector = Vector2D.generateRotatedVector(base, Rover.getViewAngle() / 2);
                    endSector = Vector2D.generateRotatedVector(endSector, r.getRotation());
                    startSector = Vector2D.generateRotatedVector(startSector, r.getRotation());
                    startSector = Vector2D.addition(startSector, center);
                    endSector = Vector2D.addition(endSector, center);

                    for (Alien a : Game.this.entityManager.getAliens()) {
                        Vector2D point = a.center();
                        if (Util.pointInViewRange(point, startSector, endSector, center)) {
                            LuaEventHandler.handleEvent(GameEvent.generateAlienOnSight(point.getX(), point.getY()), r.getLuaVM());
                        }

                    }

                    for (MetalHeap m : Game.this.entityManager.getHeaps()) {
                        Vector2D point = m.center();
                        if (Util.pointInViewRange(point, startSector, endSector, center)) {
                            LuaEventHandler.handleEvent(GameEvent.generateMetalOnSight(point.getX(), point.getY()), r.getLuaVM());
                        }
                    }

                    if (r.getBattery() < 25) {
                        LuaEventHandler.handleEvent(GameEvent.generateLowBattery(), r.getLuaVM());
                    }

                    if (r.getTarget() == null) {
                        LuaEventHandler.handleEvent(GameEvent.generateIdle(), r.getLuaVM());
                    }
                }
            };

            Runnable requestHandling = () -> {
                for (Rover r : Game.this.entityManager.getRovers()) {
                    synchronized (r.getActionRequests()) {
                        while (!r.getActionRequests().isEmpty()) {
                            switch (Objects.requireNonNull(r.getActionRequests().poll())) {
                                case MINE:
                                    for (MetalHeap m : Game.this.entityManager.getHeaps()) {
                                        if (r.getLoad() >= Game.this.roverMaxLoad) {
                                            break;
                                        }
                                        if (GameObject.isCollision(r, m) && m.isAlive()) {
                                            m.killEntity();
                                            r.mineOre();
                                        }
                                    }
                                    break;
                                default:
                                    System.err.println("Unsopperted Request");
                            }
                        }
                    }
                }
            };


            // Zeitpunkt des letzten Ticks
            private long last = System.nanoTime();
            private final double expected = 1d / 60;

            @Override
            public void handle(long now) {
                if (!running) {
                    stop(); //Note: this doesn't invoke Game.this.stop(), this stops only this timer
                    return;
                }

                if (now - last > 500_000_000) {
                    System.err.println("SYSTEM: Can't keep up");
                }

                double delta = (now - last) / 1_000_000_000d;
                double timeDiff = delta < expected ? delta : expected;

                update(timeDiff * Game.this.speed);

                Thread[] threads = {
                        new Thread(collisionDetectionFunction, "Collision Detection"),
                        new Thread(garageFunction, "Garage thread"),
                        new Thread(metalHeapCollectionFunction, "Metal Heap Collection"),
                        new Thread(resolveBorderCollision, "Resolve Border Collision"),
                        new Thread(eventCalculator, "Event Calculator"),
                        new Thread(communicationDistribution, "Communication"),
                        new Thread(requestHandling, "Request Handling")
                };


                for (Thread t : threads) {
                    t.start();
                }

                render();

                synchronized (Game.this) {
                    Game.this.fps++;
                }

                if (isGameOver()) {
                    Game.this.stop();
                    Game.this.showGameOverScreen();
                }

                for (Thread t : threads) {
                    try {
                        t.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                cleanup();
                last = now;

            }

            // Loest Berechnungen fuer den naechsten Tick aus
            private void update(double timeDiff) {
                for (List<? extends DynamicGameObject> list : Game.this.entityManager.getDynamics()) {
                    list.forEach(gameObject -> gameObject.update(timeDiff));
                }
            }

            // Stellt den naechsten Tick grafisch dar
            private void render() {
                dynamicGC.clearRect(0, 0, screenWidth, screenHeight);
                for (List<? extends DynamicGameObject> list : Game.this.entityManager.getDynamics()) {
                    list.forEach(gameObject -> gameObject.render(dynamicGC));
                }
            }

            /*
             * Der GarbageCollector welcher alle toten GameObjects entfernt
             */
            private void cleanup() {
                for (List<? extends GameObject> list : Game.this.entityManager.getAll()) {
                    list.removeIf(GameObject::isDead);
                }
            }
        };

        AnimationTimer informationUpdater = new AnimationTimer() {
            private long timer = System.nanoTime() + (long) (1_000_000_000 / Game.this.speed);
            private TimeCounter timeCounter = new TimeCounter();

            @Override
            public void handle(long now) {
                if (!running) {
                    stop();
                }
                if (now - timer >= 0) {
                    timer += 1_000_000_000 / Game.this.speed;
                    timeCounter.count();
                    Game.this.scoreLabel.setText(String.format("Score: %d", Game.this.score));
                    Game.this.metalLabel.setText(String.format("Metal in Garage: %d / %d", Game.this.getGarage().getIronNum(), Game.this.numNeededMetal));
                    Game.this.elapsedTime.setText(timeCounter.toString());
                    Game.this.roverCounter.setText(String.format("Rovers left: %d", Game.this.entityManager.getRovers().size()));
                }
            }
        };

        AnimationTimer debugger = new AnimationTimer() {
            private long timer = System.nanoTime() + 1_000_000_000;

            @Override
            public void handle(long now) {
                if (!running) {
                    stop();
                }
                if (now - timer >= 0) {
                    timer += 1_000_000_000;
                    Game.this.stage.setTitle(String.format("MarsMe - FPS: %d", Game.this.fps));
                    fps = 0;
                }
            }
        };

        /*
         * Spawnt einen neuen Rover sobald eine bestimmte Anzahl an Metall in der Garage ist
         */
        AnimationTimer roverSpawner = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (!running) {
                    stop();
                    return;
                }

                Garage garage = Game.this.getGarage();
                if (garage.getIronNum() >= Game.this.numNeededMetal) {
                    garage.remIronNum(Game.this.numNeededMetal);
                    Game.this.entityManager.getRovers().add(Rover.createCentralizedRover(Game.this.initCode, garage.center(), Game.this.roverMaxLoad, Game.this.console));
                    System.out.println("SYSTEM: Rover spawned!");
                }
            }
        };

        /*
         * Ueberprueft jeden Tick die Anzahl der Metallhaufen und erhoet sie gegbenenfalls
         */
        AnimationTimer metalHeapCountWatchdog = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (!running) {
                    stop();
                    return;
                }
                try {
                    while (Game.this.getMetalHeapCounter() < 5) {
                        Rectangle2D screen = Game.getScreenBounds();
                        double x = Util.random(50, screen.getMaxX() - 50);
                        double y = Util.random(50, screen.getMaxY() - 50);
                        MetalHeap metalHeap = MetalHeap.constructLocatedMetalHeap(new Vector2D(x, y));
                        if (!GameObject.isCollision(metalHeap, getGarage())) {
                            Game.this.entityManager.getHeaps().add(metalHeap);
                        }
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }

        };


        // Liste von zu startenden AnimationTimers
        ArrayList<AnimationTimer> animationTimerList = new ArrayList<>();

        animationTimerList.add(metalHeapCountWatchdog);
        animationTimerList.add(gameLoop);
        animationTimerList.add(roverSpawner);
        animationTimerList.add(debugger);
        animationTimerList.add(informationUpdater);

        for (AnimationTimer at : animationTimerList) {
            at.start();
        }

    }

    /**
     * Stoppt das Game
     */
    private void stop() {
        running = false;
        entityManager.clear();
    }

    private int getMetalHeapCounter() {
        return entityManager.getHeaps().size();
    }

    private boolean isGameOver() {
        return this.entityManager.getRovers().isEmpty();
    }

    /**
     * Beendet das Game und zeigt den GameOver Bildschirm
     * Wird aufgerufen wenn alle Rover tot sind
     */
    private void showGameOverScreen() {
        root.getChildren().clear();

        ImageView imageView = getCentralizedGameOverScreen();

        VBox box = new VBox();
        box.setFillWidth(true);

        scene.setFill(Color.BLACK);

        root.getChildren().add(imageView);
        System.out.println("SYSTEM: ##### Game Over #####");
        System.out.println("SYSTEM: Score: " + score);
        System.out.println("SYSTEM: Time: " + elapsedTime.getText());
    }

    private Garage getGarage() {
        return entityManager.getGarages().get(0);
    }
}
