import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import marsme.gui.MainController;
import marsme.util.IconManager;

/**
 * Die Hauptklasse, verantwortlich fuer die Ausfuehrung der App
 *
 * @author Jan, Eric
 * @version 1.0.0
 */

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("fx/main.fxml"));
        final Parent root = loader.load();
        final Scene scene = new Scene(root);

        primaryStage.setScene(scene);
        primaryStage.setTitle("MarsMe");
        primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.getIcons().addAll(IconManager.mainIcons);
        primaryStage.show();

        MainController controller = loader.getController();
        controller.init();

        primaryStage.setOnCloseRequest(event -> {
            System.exit(0);
            Platform.exit();
        });
    }
}
